set termopt enhanced    # turn on enhanced text mode
set title "Change in weights over training on AND dataset,\nfor a Neural Network with 2 inputs, 1 output,\nand 1 hidden layer with 2 nodes"
set xlabel "Round of training"
set ylabel "Weight/bias value"
set key right center
plot NaN title "b^{1}_{1}" w line lt 1 lc 5 lw 6,\
 'weightChanges2Hidden.dat' using 1:2 with lines title "" lt 1 lc 5 lw 3,\
 NaN title "b^{1}_{2}" w line lt 1 lc 4 lw 6,\
 'weightChanges2Hidden.dat' using 1:3 with lines title "" lt 1 lc 4 lw 3,\
 NaN title "W^{1}_{11}" w line lt 1 lc 3 lw 6,\
 'weightChanges2Hidden.dat' using 1:4 with lines title "" lt 1 lc 3 lw 3,\
 NaN title "W^{1}_{12}" w line lt 1 lc 2 lw 6,\
 'weightChanges2Hidden.dat' using 1:5 with lines title "" lt 1 lc 2 lw 3,\
 NaN title "W^{1}_{21}" w line lt 1 lc 1 lw 6,\
 'weightChanges2Hidden.dat' using 1:6 with lines title "" lt 1 lc 1 lw 3,\
 NaN title "W^{1}_{22}" w line lt 1 lc 6 lw 6,\
 'weightChanges2Hidden.dat' using 1:7 with lines title "" lt 1 lc 6 lw 3,\
 NaN title "b^{2}_{1}" w line lt 1 lc 7 lw 6,\
 'weightChanges2Hidden.dat' using 1:8 with lines title "" lt 1 lc 7 lw 3,\
 NaN title "W^{2}_{1}" w line lt 1 lc 8 lw 6,\
 'weightChanges2Hidden.dat' using 1:9 with lines title "" lt 1 lc 8 lw 3,\
 NaN title "W^{2}_{2}" w line lt 1 lc 9 lw 6,\
 'weightChanges2Hidden.dat' using 1:10 with lines title "" lt 1 lc 9 lw 3

