set title "Change in weights over training on AND dataset,\nfor a Neural Network with 2 inputs, 1 output, and no hidden layers"
set xlabel "Round of training"
set ylabel "Mean cost"
set key right center
plot NaN title "Bias on output" w line lt 1 lc 5 lw 6,\
 'weightChanges.dat' using 1:2 with lines title "" lt 1 lc 5 lw 3,\
 NaN title "Weight on first input" w line lt 1 lc 4 lw 6,\
 'weightChanges.dat' using 1:3 with lines title "" lt 1 lc 4 lw 3,\
 NaN title "Weight on second input" w line lt 1 lc 3 lw 6,\
 'weightChanges.dat' using 1:4 with lines title "" lt 1 lc 3 lw 3
