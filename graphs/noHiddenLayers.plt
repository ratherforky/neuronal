set title "Mean cost over training, for a Neural Network with 2 inputs, 1 output, and no hidden layers"
set xlabel "Round of training"
set ylabel "Mean cost"
set key right top
plot NaN title "AND" w line lt 1 lc 5 lw 6,\
 'noHiddenLayers.dat' using 1:2 with lines title "" lt 1 lc 5 lw 3,\
 NaN title "OR" w line lt 1 lc 4 lw 6,\
 'noHiddenLayers.dat' using 1:3 with lines title "" lt 1 lc 4 lw 3,\
 NaN title "XOR" w line lt 1 lc 3 lw 6,\
 'noHiddenLayers.dat' using 1:4 with lines title "" lt 1 lc 3 lw 3
