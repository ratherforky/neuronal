module Main

-- Inspired by https://blog.jle.im/entry/practical-dependent-types-in-haskell-1.html

import Data.Vect
import Data.Matrix
import Data.Matrix.Numeric

import Effects
import Effect.StdIO
import Effect.Random

-- y = b + Wx
record Weights (i : Nat) (o : Nat) where
  constructor MkWeights
  biases : Vect o Double
  weights  : Matrix o i Double

infixr 5 ::~

implementation Show (Weights i o) where
  show ws = "Biases:\n" ++ show (biases ws)    ++ "\n" ++
            "Weights:\n"  ++ showNewlineMatrix (weights ws) ++ "\n"
    where
      showNewlineMatrix = unlines . toList . map show

implementation Default (Weights i o) where
  default = MkWeights default default

implementation Num (Weights i o) where
  (MkWeights wB1 wN1) + (MkWeights wB2 wN2) = MkWeights (wB1 + wB2) (zipWith (+) wN1 wN2)

  (MkWeights wB1 wN1) * (MkWeights wB2 wN2) = MkWeights (wB1 * wB2) (zipWith (*) wN1 wN2)
  fromInteger x = MkWeights (replicate _ (fromInteger x)) (replicate _ (replicate _ (fromInteger x)))

implementation Fractional a => Fractional (Vect n a) where
  (/) = zipWith (/)

implementation Fractional (Weights i o) where
  (MkWeights wB1 wN1) / (MkWeights wB2 wN2) = MkWeights (wB1 / wB2) (zipWith (/) wN1 wN2)

interface SpaceDelimited a where
  spaceDelimit : a -> String

implementation SpaceDelimited (Weights i o) where
  spaceDelimit ws = unwords $ map show (toList $ biases ws) 
                     ++ map (unwords . map show) (vecMatToListMat $ weights ws)
  where
    vecMatToListMat : Matrix _ _ a -> List (List a)
    vecMatToListMat = toList . map toList


-- i = input
-- o = output
-- h = hidden
data Network : (input : Nat) 
            -> (hidden : List Nat)
            -> (output : Nat)
            -> Type 
  where
    O : Weights i o  -> Network i [] o
    (::~) : Weights i h -> Network h hs o -> Network i (h :: hs) o

-- foldNetworkl : (func : acc -> Weights _ _ -> acc) -> (init : acc) -> (input : Network i hs o) -> acc
-- foldNetworkl func init (O ws) = go func init ws
-- foldNetworkl func init (ws ::~ net) = foldNetworkl func (func init ws) net
--   where
--     go : acc -> acc
--     go acc 

indent : Nat -> String -> String
indent n = unlines . map (\str => spaces n ++ str) . lines 
  where
    spaces : Nat -> String
    spaces n = pack (Prelude.List.replicate n ' ')

implementation Show (Network i hs o) where
  show {hs} net = go (fromNat (length hs + 1)) net
    where
      go : Int -> Network _ _ _ -> String
      go numLayers (O ws) =
        concat (the (List String) ["Output layer:\n", show ws, "\n"])
      go numLayers (ws ::~ network) =
        concat (the (List String) ["Layer ", show (layer ((ws ::~ network))), ":\n", 
                                   show ws, "\n", 
                                   go numLayers network])
        where
          layer : Network _ foos _ -> Int
          layer {foos} _ = numLayers - fromNat (length foos)

implementation SpaceDelimited (Network i hs o) where
  spaceDelimit (O ws)       = spaceDelimit ws
  spaceDelimit (ws ::~ net) = spaceDelimit ws ++ " " ++ spaceDelimit net

implementation Default (Network i hs o) where
  default {hs = []}      = O default
  default {hs = x :: xs} = default ::~ default

implementation Num (Network i hs o) where
  (O w1)      + (O w2)      = O (w1 + w2)
  (w1 ::~ n1) + (w2 ::~ n2) = (w1 + w2) ::~ (n1 + n2)

  (O w1)      * (O w2)      = O (w1 * w2)
  (w1 ::~ n1) * (w2 ::~ n2) = (w1 * w2) ::~ (n1 * n2)

  fromInteger {hs = []}      n = O (fromInteger n)
  fromInteger {hs = x :: xs} n = fromInteger n ::~ fromInteger n 

implementation Fractional (Network i hs o) where
  (O w1)      / (O w2)      = O (w1 / w2)
  (w1 ::~ n1) / (w2 ::~ n2) = (w1 / w2) ::~ (n1 / n2)

-- Adapted from: https://bl.ocks.org/justinmanley/f2e169feb32e06e06c2f
-- Naively generate a (pseudo)-random float between 0 and 1.
randomDouble : Eff Double [RND]
randomDouble = let max = 10000000 in do
	rnd <- rndInt (-max) max
	pure (fromInteger rnd / fromInteger max)

randomVect : Eff (Vect n Double) [RND]
randomVect = mkRandomVect _
  where
    mkRandomVect : (n : Nat) -> Eff (Vect n Double) [RND]
    mkRandomVect Z     = pure []
    mkRandomVect (S k) = pure (!randomDouble :: !(mkRandomVect k))

randomMatrix : Eff (Matrix n m Double) [RND]
randomMatrix = mkRandomMatrix _ _
  where
    mkRandomMatrix : (n : Nat) -> (m : Nat) -> Eff (Matrix n m Double) [RND]
    mkRandomMatrix Z     _ = pure []
    mkRandomMatrix (S k) m = pure (!randomVect :: !(mkRandomMatrix k m))

randomWeights : Eff (Weights i o) [RND]
randomWeights = pure (MkWeights !randomVect !randomMatrix)

randomNetwork : Effects.SimpleEff.Eff (Network i hs o) [RND]
randomNetwork {hs = []}      = pure (O !randomWeights)
randomNetwork {hs = x :: xs} = pure (!randomWeights ::~ !randomNetwork) 

-- Running and updating networks

sigmoid : Double -> Double
sigmoid x = 1 / (1 + exp (-x))

-- Differential of sigmoid
sigmoid' : Double -> Double
sigmoid' x = sigmoid x * (1 - sigmoid x)

-- y = b + Wx
-- b = vector of biases
-- W = weight matrix for incoming neurons
-- x = vector of incoming neurons
-- y = vector of activations in outgoing neurons
-- Find y
runLayer : Weights i o -> Vect i Double -> Vect o Double
runLayer (MkWeights b w) x = b + (w </> x)

-- runLayer : Weights n m -> Vect n Double -> Vect m Double
-- runLayer (MkWeights b w) x = b + (w </> x)

runNet : Network i hs o -> Vect i Double -> Vect o Double
-- O : Weights i o  -> Network i [] o
runNet (O w) v        = map sigmoid (runLayer w v)
-- (::~) : Weights i h -> Network h hs o -> Network i (h :: hs) o
runNet (w ::~ net') v = runNet net' v'
  where
    v' = map sigmoid (runLayer w v)

cost : Vect o Double -> Vect o Double -> Double
cost {o} output target = sum (difference * difference)
  where
    difference : Vect o Double
    difference = output - target


-- Basic gradient descent algorithm:
-- Compute dC
-- Small step in -dC direction
-- Repeat

networkAdjustments : Double          -- Learning rate
                  -> Vect i Double   -- Input vector
                  -> Vect o Double   -- Target vector
                  -> Network i hs o  -- Network being trained
                  -> Network i hs o
networkAdjustments {o} learningRate input target = fst . go input
  where
    go : Vect i Double  -- Input vector
      -> Network i hs o -- Network to train
      -> (Network i hs o, Vect i Double) -- Updated Network with adjustments to input
    go {i} aᴸˢᵘᵇ¹ (O w) =
      let
        -- zᴸ = wᴸ * aᴸˢᵘᵇ¹ + bᴸ
        zᴸ = runLayer w aᴸˢᵘᵇ¹ -- Get output layer
        dzᴸ_dwᴸ = aᴸˢᵘᵇ¹
        dzᴸ_dbᴸ = 1

        -- aᴸ = sigmoid zᴸ
        aᴸ = map sigmoid zᴸ   -- Push outputs through sigmoid to get activations
        daᴸ_dzᴸ = map sigmoid' zᴸ

        -- C = (aᴸ - y)^2
        dC_daᴸ = 2 * (aᴸ - target)
        dC_dzᴸ = dC_daᴸ * daᴸ_dzᴸ

        -- dC_dbᴸ = dC_dzᴸ * dzᴸ_dbᴸ
        -- But dzᴸ_dbᴸ = 1, so
        dC_dbᴸ = dC_dzᴸ

        -- dC/dwᴸ using the chain rule. aka the gradient of weights to the Cost
        dC_dwᴸ = dC_dzᴸ >< dzᴸ_dwᴸ

        biasesAdj = -learningRate <#  dC_dbᴸ
        weightsAdj  = -learningRate <#> dC_dwᴸ
        w' = MkWeights biasesAdj weightsAdj

        dC_daᴸˢᵘᵇ¹ = (transpose (weights w)) </> dC_dzᴸ
      in (O w', dC_daᴸˢᵘᵇ¹)
    go {i} {hs=h::hs'} aᴸˢᵘᵇ¹ (w ::~ n) =
      let
        -- zᴸ = wᴸ * aᴸˢᵘᵇ¹ + bᴸ
        zᴸ = runLayer w aᴸˢᵘᵇ¹ -- Get output layer
        dzᴸ_dwᴸ = aᴸˢᵘᵇ¹
        dzᴸ_dbᴸ = 1
        
        -- aᴸ = sigmoid zᴸ
        aᴸ = map sigmoid zᴸ   -- Push outputs through sigmoid to get activations
        daᴸ_dzᴸ = map sigmoid' zᴸ

        -- Recursively get updated network and changes to weights
        (n', dC_daᴸ) = go aᴸ n
        dC_dzᴸ = dC_daᴸ * daᴸ_dzᴸ

        -- dC_dbᴸ = dC_dzᴸ * dzᴸ_dbᴸ
        -- But dzᴸ_dbᴸ = 1, so
        dC_dbᴸ = dC_dzᴸ

        -- dC/dwᴸ using the chain rule. aka the gradient of weights to the Cost
        dC_dwᴸ = dC_dzᴸ >< dzᴸ_dwᴸ

        biasesAdj = -learningRate <#  dC_dbᴸ
        weightsAdj  = -learningRate <#> dC_dwᴸ
        w' = MkWeights biasesAdj weightsAdj

        dC_daᴸˢᵘᵇ¹ = (transpose (weights w)) </> dC_dzᴸ
      in (w' ::~ n', dC_daᴸˢᵘᵇ¹)

chunk : Nat -> List a -> List (List a)
chunk size [] = []
chunk size xs = let (xsSize, xsRest) = Prelude.List.splitAt size xs
                in xsSize :: chunk size xsRest

mean : Fractional a => List a -> a
mean xs = sum xs / (fromInteger . toIntegerNat) (length xs)

oneStepWithSet : Double
              -> Network i hs o
              -> List (Vect i Double, Vect o Double)
              -> Network i hs o
oneStepWithSet {i} {hs} {o} lr network = (network +) . mean . map getAdjustments
  where
    getAdjustments : (Vect i Double, Vect o Double) -> Network i hs o
    getAdjustments (input, output) = networkAdjustments lr input output network

trainWithSet : Double
            -> Nat
            -> Network i hs o
            -> List (Vect i Double, Vect o Double)
            -> Network i hs o
trainWithSet {i} {hs} {o} lr batchSize network = foldl f network . chunk batchSize
  where
    f : Network i hs o -> List (Vect i Double, Vect o Double) -> Network i hs o
    f n = (n +) . mean . map getAdjustments 
      where
        getAdjustments : (Vect i Double, Vect o Double) -> Network i hs o
        getAdjustments (input, output) = networkAdjustments lr input output n

-- Training data

andTable : List (Vect 2 Double, Vect 1 Double)
andTable = [ ([0,0],[0])
          , ([0,1],[0])
          , ([1,0],[0])
          , ([1,1],[1])
          ]

orTable : List (Vect 2 Double, Vect 1 Double)
orTable =
  [ ([0,0],[0])
  , ([0,1],[1])
  , ([1,0],[1])
  , ([1,1],[1])
  ]

xorTable : List (Vect 2 Double, Vect 1 Double)
xorTable =
  [ ([0,0],[0])
  , ([0,1],[1])
  , ([1,0],[1])
  , ([1,1],[0])
  ]

-- Networks

vsimpleNetwork : Network 2 [] 1
vsimpleNetwork = O $ MkWeights [0] [[0.5, 0.5]]

simpleNetwork : Network 2 [2] 1
simpleNetwork = default

randAndNetwork : Eff (Network 2 [5] 1) [RND]
randAndNetwork = randomNetwork

replicateData : Nat -> List (Vect i Double, Vect o Double) -> List (Vect i Double, Vect o Double)
replicateData n xs = concat $ Prelude.List.replicate n xs

logicGateNetwork : Double
                -> Nat
                -> List (Vect 2 Double, Vect 1 Double)
                -> Eff (Network 2 hs 1) [RND]
logicGateNetwork lr numRepeats table = pure (trainWithSet lr 4 !randomNetwork xs)
  where
    xs : List (Vect 2 Double, Vect 1 Double)
    xs = replicateData numRepeats table

andNetwork : Eff (Network 2 hs 1) [RND]
andNetwork = logicGateNetwork 0.5 1000 andTable

andNetwork1 : Eff (Network 2 [5] 1) [RND]
andNetwork1 = andNetwork

xorNetwork : Eff (Network 2 hs 1) [RND]
xorNetwork = logicGateNetwork 0.5 10000 xorTable

xorNetwork5 : Eff (Network 2 [5] 1) [RND]
xorNetwork5 = xorNetwork

orNetwork : Eff (Network 2 hs 1) [RND]
orNetwork = logicGateNetwork 0.5 1000 orTable

showOffNet : Network 2 hs 1 -> Eff (List ((Int,Int), (Vect 1 Double))) [RND]
showOffNet network = pure $
  zip [(0,0),(0,1),(1,0),(1,1)]
      (map {f=List} (runNet network) [[0,0],[0,1],[1,0],[1,1]])

showNet : Network 2 hs 1 -> Eff String [RND]
showNet net = Effects.(<$>) (unlines . map show) (showOffNet net)

printLnList : Show a => List a -> Eff () [RND,STDIO]
printLnList []      = pure ()
printLnList (x::xs) = do 
  printLn x
  printLnList xs

putStrLnList : List String -> Eff () [RND,STDIO]
putStrLnList []      = pure ()
putStrLnList (x::xs) = do
  putStrLn x
  putStrLnList xs

showOffAllNet : Eff () [RND,STDIO]
showOffAllNet = do
  srand 7653452342352
  putStrLn "AND Network 2 [] 1:"
  putStrLn !(showNet (the (Network 2 [] 1) !andNetwork))
  putStrLn "AND Network 2 [1] 1:"
  putStrLn !(showNet (the (Network 2 [1] 1) !andNetwork))
  putStrLn "AND Network 2 [2] 1:"
  putStrLn !(showNet (the (Network 2 [2] 1) !andNetwork))
  putStrLn "AND Network 2 [3] 1:"
  putStrLn !(showNet (the (Network 2 [3] 1) !andNetwork))
  putStrLn "AND Network 2 [4] 1:"
  putStrLn !(showNet (the (Network 2 [4] 1) !andNetwork))
  putStrLn "AND Network 2 [5] 1:"
  putStrLn !(showNet (the (Network 2 [5] 1) !andNetwork))
  putStrLn "AND Network 2 [6] 1:"
  putStrLn !(showNet (the (Network 2 [6] 1) !andNetwork))
  putStrLn "OR Network 2 [] 1:"
  putStrLn !(showNet (the (Network 2 [] 1) !orNetwork))
  putStrLn "OR Network 2 [1] 1:"
  putStrLn !(showNet (the (Network 2 [1] 1) !orNetwork))
  putStrLn "OR Network 2 [2] 1:"
  putStrLn !(showNet (the (Network 2 [2] 1) !orNetwork))
  putStrLn "OR Network 2 [3] 1:"
  putStrLn !(showNet (the (Network 2 [3] 1) !orNetwork))
  putStrLn "OR Network 2 [4] 1:"
  putStrLn !(showNet (the (Network 2 [4] 1) !orNetwork))
  putStrLn "OR Network 2 [5] 1:"
  putStrLn !(showNet (the (Network 2 [5] 1) !orNetwork))
  putStrLn "OR Network 2 [6] 1:"
  putStrLn !(showNet (the (Network 2 [6] 1) !orNetwork))
  putStrLn "XOR Network 2 [] 1:"
  putStrLn !(showNet (the (Network 2 [] 1) !xorNetwork))
  putStrLn "XOR Network 2 [1] 1:"
  putStrLn !(showNet (the (Network 2 [1] 1) !xorNetwork))
  putStrLn "XOR Network 2 [2] 1:"
  putStrLn !(showNet (the (Network 2 [2] 1) !xorNetwork))
  putStrLn "XOR Network 2 [3] 1:"
  putStrLn !(showNet (the (Network 2 [3] 1) !xorNetwork))
  putStrLn "XOR Network 2 [4] 1:"
  putStrLn !(showNet (the (Network 2 [4] 1) !xorNetwork))
  putStrLn "XOR Network 2 [5] 1:"
  putStrLn !(showNet (the (Network 2 [5] 1) !xorNetwork))
  putStrLn "XOR Network 2 [6] 1:"
  putStrLn !(showNet (the (Network 2 [6] 1) !xorNetwork))

-- foo : String -> Network 2 _ 1 -> List Nat -> Eff String [RND]
-- foo logicGate network [] = pure ""
-- foo logicGate network (x::xs) = pure $ unlines 
--   [ logicGate ++ ": Network 2 [" ++ show x ++ "] 1"
--   , !(showNet (the (Network 2 [x] 1) network))
--   , !(foo logicGate network xs)
--   ]

learningOverTime : Double
                -> Nat
                -> List (Vect i Double, Vect o Double)
                -> Network i hs o
                -> List (Nat, Network i hs o)
learningOverTime lr numRepeats dat network =
  reverse $ foldl f [(0, network)] (Prelude.List.replicate numRepeats dat)
  where
    f : List (Nat, Network i hs o)
     -> List (Vect i Double, Vect o Double)
     -> List (Nat, Network i hs o)
    f [] _ = []
    f acc@((n, net)::accs) trainingData = (n+1, oneStepWithSet lr net trainingData) :: acc

meanCostOfNetwork : Network i hs o -> List (Vect i Double, Vect o Double) -> Double
meanCostOfNetwork {i} {o} net targets = mean $ map f targets
  where
    f : (Vect i Double, Vect o Double) -> Double
    f (input, target) = cost (runNet net input) target 

plotNetwork : Double
           -> Nat
           -> List (Vect i Double, Vect o Double)
           -> Network i hs o
           -> List (Nat, Double)
plotNetwork lr numRepeats dat network = 
    map (\(n, net) => (n, meanCostOfNetwork net dat)) 
  $ learningOverTime lr numRepeats dat network

graphNetwork : List (Vect i Double, Vect o Double)
            -> Network i hs o
            -> Eff (List (Nat, Double)) [RND]
graphNetwork dat network = pure $ plotNetwork 1 2500 dat network

typeListGraphNet : List (Vect i Double, Vect o Double) 
                -> List Nat
                -> Eff (List (List Nat, List (Nat, Double))) [RND]
typeListGraphNet {i} {o} dat []      = pure $ [([], !(graphNetwork dat (the (Network i [] o) !randomNetwork)))]
typeListGraphNet {i} {o} dat (n::ns) =
  pure $ ([n], !(graphNetwork dat (the (Network i [n] o) !randomNetwork)))
      :: !(typeListGraphNet dat ns)

zipSnd3 : List (a, b) -> List (a, c) -> List (a, d) -> List (a, b, c, d)
zipSnd3 bs cs ds = map (\((n,x),y,z) => (n,x,y,z)) $ zip3 bs (map snd cs) (map snd ds)

unlinesMap : (a -> String) -> List a -> String
unlinesMap f = unlines . map f

formatData : List (Nat, Double) -> String
formatData = unlinesMap pretty
  where
    pretty : (Nat, Double) -> String
    pretty (n, d) = show n ++ " " ++ show d

formatData3 : List (Nat, Double, Double, Double) -> String
formatData3 = unlinesMap pretty
  where
    pretty : (Nat, Double, Double, Double) -> String
    pretty (n, x, y, z) = show n ++ " "
                       ++ show x ++ " "
                       ++ show y ++ " "
                       ++ show z

bar : List (List Nat, List (Nat, Double))
   -> List (List Nat, List (Nat, Double))
   -> List (List Nat, List (Nat, Double))
   -> List (List Nat, List (Nat, Double, Double, Double))
bar [] _ _ = []
bar _ [] _ = []
bar _ _ [] = []
bar ((n, xs)::xss) ((_, ys)::yss) ((_, zs)::zss) = (n, zipSnd3 xs ys zs)::(bar xss yss zss)

foo : Eff (List (List Nat, String)) [RND]
foo = do
  and <- typeListGraphNet andTable [1,2]
  or  <- typeListGraphNet orTable  [1,2]
  xor <- typeListGraphNet xorTable [1,2]
  pure $ map (\(hs, xs) => (hs, formatData3 xs))
       $ bar and or xor

baz : (List Nat, String) -> IO ()
baz ([], str) = do
  posErr <- writeFile ("graphs/noHiddenLayers.dat") str
  case posErr of
    Left e   => print e
    Right () => pure ()
baz ([n], str) = do
  posErr <- writeFile ("graphs/" ++ show n ++ "HiddenLayers.dat") str
  case posErr of
    Left e   => print e
    Right () => pure ()

makeGraphs : IO ()
makeGraphs = do
  xs <- run (do srand 121241897985308169
                foo)
  traverse_ baz xs

weightChanges : IO ()
weightChanges =
  let 
    fooy = unlines
         $ map (\(n, net) => show n ++ " " ++ spaceDelimit net)
         $ learningOverTime 1 1000 andTable simpleNetwork
  in ignore $ writeFile "graphs/weightChanges2Hidden.dat" fooy

main : IO ()
main = do
  -- run $ showOffAllNet
  -- makeGraphs
  weightChanges
